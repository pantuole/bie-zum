(define (domain artificialProgrammer)
    (:requirements
        :strips
        :typing
        :negative-preconditions
    )

    (:types register memAddress char)

    (:constants
        AX - register
        BX - register
        CX - register
    )

    (:predicates
        (registerValue ?register - register ?value - char)
        (memAt ?memAddr - memAddress ?value - char)
    )

    (:action load
        :parameters (?memAddr - memAddress ?r - register ?value - char)
        :precondition (and (memAt ?memAddr ?value))
        :effect (and (registerValue ?r ?value))
    )

    (:action save
        :parameters (?memAddr - memAddress ?r - register ?value - char)
        :precondition (and (registerValue ?r ?value))
        :effect (and (memAt ?memAddr ?value))
    )

    (:action swap
        :parameters (?r1 - register ?r2 - register, ?v1 - char ?v2 - char)
        :precondition (and
                            (registerValue ?r1 ?v1)
                            (registerValue ?r2 ?v2)
                      )
        :effect (and
                    (registerValue ?r1 ?v2)
                    (registerValue ?r2 ?v1)
                )
    )

    (:action shift
        :parameters (?v1 - char ?v2 - char ?v3 - char)
        :precondition (and
                            (registerValue AX ?v1)
                            (registerValue BX ?v2)
                            (registerValue CX ?v3)
                      )
        :effect (and
                    (registerValue AX ?v2)
                    (registerValue BX ?v3)
                    (registerValue CX ?v1)
                )
    )
)