export abstract class IState {
    protected key = "";

    abstract isValid(): boolean;

    abstract next(): Iterable<IState>;

    abstract equals(other: IState): boolean;

    isGoal(goal: IState): boolean {
        return this.equals(goal);
    }

    abstract toString(): string;

    abstract heuristic(goal: IState): number;

    getKey(): string {
        if (!this.key) this.key = this.toString();
        return this.key;
    }
}

export class MachineState extends IState {
    PamStr: string = "";

    constructor(private registers: Uint8Array,
                private Pam: Uint8Array) {
        super();
        this.setKeys();
    }

    private setKeys() {
        this.PamStr = this.Pam.toString();
        this.key = this.PamStr + " " + this.registers.toString();
        return this;
    }

    equals(other: MachineState): boolean {
        return this.key === other.getKey();
    }

    getKey(): string {
        return this.key;
    }

    toString(): string {
        return this.key;
    }

    isGoal(goal: MachineState): boolean {
        return this.PamStr.slice(0, goal.PamStr.length) === goal.PamStr;
    }

    heuristic(goal: MachineState): number {
        let c = goal.Pam.length;
        for (let i = 0; i < goal.Pam.length; ++i) {
            if (goal.Pam[i] === this.Pam[i]) c--;
        }
        return c;
    }

    isValid() {
        return true;
    }

    private copy() {
        return new MachineState(
            new Uint8Array(this.registers),
            new Uint8Array(this.Pam)
        );
    }

    private load(register: number, memoryAddress: number) {
        this.registers[register] = this.Pam[memoryAddress];
        return this.setKeys();
    }

    private save(register: number, memoryAddress: number) {
        this.Pam[memoryAddress] = this.registers[register];
        return this.setKeys();
    }

    private swap(r1: number, r2: number) {
        const tmp = this.registers[r1];
        this.registers[r1] = this.registers[r2];
        this.registers[r2] = tmp;
        return this.setKeys();
    }

    private shift() {
        // AX -> CX, BX -> AX, CX -> BX
        const tmp = this.registers[0];
        this.registers[0] = this.registers[1];
        this.registers[1] = this.registers[2];
        this.registers[2] = tmp;
        return this.setKeys();
    }

    * next(): Iterable<IState> {
        for (let i = 0; i < this.registers.length; i++) {
            for (let j = 0; j < this.Pam.length; j++) {
                yield this.copy().load(i, j);
            }
        }
        for (let i = 0; i < this.registers.length; i++) {
            for (let j = 0; j < this.registers.length; j++) {
                yield this.copy().swap(i, j);
            }
        }
        for (let i = 0; i < this.registers.length; i++) {
            for (let j = 0; j < this.Pam.length; j++) {
                yield this.copy().save(i, j);
            }
        }
        yield this.copy().shift();
    }
}