const createPDDL = (objects: string, init: string, goal: string) => `(define (problem artificialProgrammerAsm)
    (:domain artificialProgrammer)
    (:requirements :strips :typing)

    (:objects
        ${objects}
    )

    (:init
        ${init}
    )

    (:goal
        (and ${goal})
    )
)`;

const normalize = (c: string) => 'c' + c.charCodeAt(0);

function main() {
    const initStr = "Pavel zjistil, že Petr neumí UI, a proto ho vyhodil od zkoušky.";
    const goalStr = "Petr zjistil, že Pavel neumí UI, a proto ho vyhodil od zkoušky.";

    if (initStr.length < goalStr.length) {
        console.error("Initial and goal lengths are different!");
        return;
    }

    const initChars = initStr.split('');
    const charsDefinition = [...new Set(initChars)].map(c => `${normalize(c)} - char`);
    const memoryAddresses = initChars.map((_, i) => `P${i} - memAddress`);
    const initialConditions = initChars.map((c, i) => `(memAt P${[i]} ${normalize(c)})`);
    const goalConditions = goalStr.split('').map((c, i) => `(memAt P${[i]} ${normalize(c)})`);

    const pddl = createPDDL(
        memoryAddresses.join(' ') + "\n\t\t" + charsDefinition.join(' '),
        initialConditions.join(' '),
        goalConditions.join(' ')
    );
    console.log(pddl);
}

main();