import { bfs, IState } from "../common/bfs"

const GOAT = "GOAT";
const WOLF = "WOLF";
const FARMER = "FARMER";
const CABBAGE = "CABBAGE";

class State implements IState {
    private readonly farmerOnBank1: boolean;

    constructor(
        private readonly bank1: string[],
        private readonly bank2: string[]
    ) {
        this.farmerOnBank1 = bank1.includes(FARMER);
    }

    * next(): Iterable<State> {
        const items = this.farmerOnBank1 ? this.bank1 : this.bank2;
        const noFarmer = items.filter(item => item != FARMER);
        if (this.farmerOnBank1) {
            yield new State(noFarmer, [FARMER, ...this.bank2]);
            for (const item of noFarmer) {
                yield new State(noFarmer.filter(i => i != item), [item, FARMER, ...this.bank2]);
            }
        } else {
            yield new State([FARMER, ...this.bank1], noFarmer);
            for (const item of noFarmer) {
                yield new State([item, FARMER, ...this.bank1], noFarmer.filter(i => i != item));
            }
        }
    }

    private static isNotValid(items: string[]) {
        return !items.includes(FARMER) &&
            ((items.includes(GOAT) && items.includes(CABBAGE)) ||
                (items.includes(GOAT) && items.includes(WOLF)))
    }

    isValid() {
        return !State.isNotValid(this.bank1) && !State.isNotValid(this.bank2);
    }

    equals(other: State) {
        return (
            this.bank1.length === other.bank1.length &&
            this.bank1.every((item) => other.bank1.includes(item))
        );
    }

    toString() {
        return "( [ " +
            this.bank1.sort().reduce((acc, item) => item + " " + acc, "") + "] [ " +
            this.bank2.sort().reduce((acc, item) => item + " " + acc, "")
            + "] )";
    }
}

const initialState = new State([FARMER, CABBAGE, GOAT, WOLF], []);
const finalState = new State([], [FARMER, CABBAGE, GOAT, WOLF]);

bfs(initialState, finalState, true);
