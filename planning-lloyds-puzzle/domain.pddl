(define (domain lloydsPuzzle)
    (:requirements
        :strips
        :negative-preconditions
    )

    (:predicates
        (empty ?position)
        (isNumber ?number)
        (isPosition ?position)
        (hasNumberAt ?number ?position)
        (neighbours ?position1 ?position2)
    )

    (:action moveBlock
        :parameters (?number ?from ?to)
        :precondition (and
                            (empty ?to)
                            (isNumber ?number)
                            (isPosition ?from)
                            (isPosition ?to)
                            (hasNumberAt ?number ?from)
                            (or (neighbours ?from ?to) (neighbours ?to ?from))
                        )
        :effect (and
                    (empty ?from)
                    (not (empty ?to))
                    (hasNumberAt ?number ?to)
                    (not (hasNumberAt ?number ?from))
                )
    )
)