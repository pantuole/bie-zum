const generateProblemPddl = (objects: string, init: string, goal: string) => `(define (problem lloydsPuzzle15)
    (:domain lloydsPuzzle)
    
    (:requirements :strips)
    
    (:objects
        ${objects}
    )
    
    (:init 
        ${init}
    )
    
    (:goal
        ${goal}
    )
)
`;

const digits = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

const nameFromNumber = (n: number) => (
    n.toString().split('').map((dIndex) => digits[+dIndex]).join('-')
);

const positionName = (x: number, y: number) => (
    `pos-${nameFromNumber(x)}-${nameFromNumber(y)}`
);

const isValidPosition = (x: number, y: number, gridSize: number) => (
    x >= 1 && y >= 1 && x <= gridSize && y <= gridSize
);

const offsets = [[1, 0], [-1, 0], [0, 1], [0, -1]];

function* neighbours(x: number, y: number, gridSize: number) {
    for (const [xOffset, yOffset] of offsets) {
        if (isValidPosition(x + xOffset, y + yOffset, gridSize)) {
            yield [x + xOffset, y + yOffset];
        }
    }
}

async function main() {
    const gridFileName = process.argv[2];
    const gridFile = Bun.file(gridFileName);
    const fileExists = await gridFile.exists();
    if (!fileExists) {
        console.error("Grid file is invalid.")
        return;
    }

    let gridSize = 0;
    const goals: string[] = [];
    const rules: string[] = [];
    const objects: string[] = [];
    const initialConditions: string[] = [];

    let rowIndex = 0, columnIndex = 0;
    const text = await gridFile.text();
    for (const line of text.split("\n")) {
        columnIndex = 0;
        rowIndex++;
        const row = line.split(" ");
        gridSize = row.length;
        for (const number of row) {
            columnIndex++;
            const position = positionName(columnIndex, rowIndex);
            if (number === "-") {
                initialConditions.push(`(empty ${position})`);
                continue;
            }
            if (+number < 1 || +number > gridSize * gridSize) {
                console.error("Invalid grid.")
                return;
            }
            const name = nameFromNumber(+number);
            rules.push(`(isNumber ${name})`);
            objects.push(name);
            initialConditions.push(`(hasNumberAt ${name} ${position})`);
        }
    }

    const neighborsAdded = new Set<string>();

    for (let i = 1; i <= gridSize; i++) {
        for (let j = 1; j <= gridSize; j++) {
            const name = positionName(i, j);
            neighborsAdded.add(name);
            objects.push(name);
            rules.push(`(isPosition ${name})`);
            for (const [nx, ny] of neighbours(i, j, gridSize)) {
                const neighborName = positionName(nx, ny);
                if (neighborsAdded.has(neighborName)) continue;
                rules.push(`(neighbours ${name} ${neighborName})`);
            }
        }
    }

    let counter = 0;
    for (let i = 1; i <= gridSize; i++) {
        for (let j = 1; j <= gridSize; j++) {
            if (i === gridSize && j === gridSize) continue;
            const goalNumber = nameFromNumber(++counter);
            const goalPosition = positionName(j, i);
            goals.push(`(hasNumberAt ${goalNumber} ${goalPosition})`);
        }
    }

    const pddl =generateProblemPddl(
        objects.join(" "),
        [...rules, ...initialConditions].join(" "),
        "(and\n\t\t\t" + goals.join(" ") + "\n\t\t)"
    );
    console.log(pddl);
}

await main();

export {};
