//-------------------------------------------------------------------
//
//  Template for solving the Travelling Salesman Problem
//  (c) 2021 Ladislava Smítková Janků <ladislava.smitkova@fit.cvut.cz>
//
//-------------------------------------------------------------------

#ifndef GENETIC_H
#define GENETIC_H

#include "tmatrix.h"
#include <vector>

typedef enum {
    CROSSOVER_METHOD_OX, CROSSOVER_METHOD_PMX
} TCrossoverMethod;

struct Config {
    int population = 0;
    int generations = 0;
    int eliteIndividuals = 0;
    double probabilityCrossover = 0;
    double probabilityMutationStep = 0;
    TCrossoverMethod crossoverMethod = CROSSOVER_METHOD_OX;
};

std::vector<int> salesmanProblemGenetic(TMatrix *matrix, const Config& config);

#endif // GENETIC_H
