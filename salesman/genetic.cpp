//---------------------------------------------------------------------------
//
//  Template for solving the Travelling Salesman Problem
//  (c) 2021 Ladislava Smítková Janků <ladislava.smitkova@fit.cvut.cz>
//
//  genetic.cpp: Implementation of genetic algorithm with these parameters:
//
//  Population:        500 individuals (can be modified by POPULATION constant)
//  Generations:       30 (can be modified by GENERATIONS constant)
//  Crossover method:  OX or PMX
//  Mutation method:   reversion of the path
//
//  Crossover probability:    95%  (PROBABILITY_CROSSOVER)
//  Mutation probability:     stepped by 5%  (PROBABILITY_MUT_STEP)
//
//  If the fitness value of the actual generation is better than last one,
//  mutation probability will be set to zero. In other case, mutation
//  probability will be increased by PROBABILITY_MUT_STEP.
//
//---------------------------------------------------------------------------

#include "genetic.h"

#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <limits>
#include <unordered_map>
#include <unordered_set>
#include <ctime>

#include "path.h"

struct TIndividual {
    std::vector<int> path;
    double fitness = 0;
    double Pr = 0;
    double q = 0;
};

std::vector<TIndividual> individuals;

bool compareByFitness(const TIndividual &a, const TIndividual &b) {
    return a.fitness > b.fitness;
}

double computeFitness(const std::vector<int> &path, TMatrix *matrix) {
    // Fitness Function (also known as the Evaluation Function) evaluates how
    // close a given solution is to the optimum solution of the desired problem.
    // It determines how fit a solution is.
    return matrix->getMaxPossibleDistance((int) path.size()) - calculatePathLength(path, matrix);
}

void recalculate(TMatrix *matrix) {
    // calculate fitness and Pr
    double sum_fitness = 0;
    for (auto &individual: individuals) {
        individual.fitness = computeFitness(individual.path, matrix);
        sum_fitness += individual.fitness;
    }

    // sort population by fitness
    std::sort(individuals.begin(), individuals.end(), compareByFitness);

    // compute Pr_i and q_i
    double q = 0;
    for (auto &individual: individuals) {
        individual.Pr = individual.fitness / sum_fitness;
        q += individual.Pr;
        individual.q = q;
    }
}

size_t rouletteWheelSelection() {
    const auto r = drand48();
    for (size_t i = 0; i < individuals.size(); ++i) {
        if (individuals[i].q >= r) {
            return i;
        }
    }
    return 0;
}

void fill(TIndividual &child, const TIndividual &parent, size_t crossoverPoint1, size_t crossoverPoint2) {
    std::unordered_set<int> used;
    used.insert(child.path[0]);
    for (size_t i = crossoverPoint1; i <= crossoverPoint2; ++i) {
        used.insert(child.path[i]);
    }
    size_t parentIndex = 0;
    for (size_t i = (crossoverPoint2 + 1) % child.path.size(); i != crossoverPoint1; i = (i + 1) % child.path.size()) {
        if (i == 0) continue;
        while (used.count(parent.path[parentIndex]) > 0) ++parentIndex;
        child.path[i] = parent.path[parentIndex];
        used.insert(parent.path[parentIndex]);
    }
}

std::pair<size_t, size_t> crossoverPoints(size_t range) {
    range -= 1;
    size_t point1 = (rand() % range) + 1, point2;
    do {
        point2 = (rand() % range) + 1;
    } while (point1 == point2);
    if (point1 > point2) {
        std::swap(point1, point2);
    }
    return std::make_pair(point1, point2);
}

std::pair<TIndividual, TIndividual> doCrossoverOX(const TIndividual &parent1, const TIndividual &parent2) {
    TIndividual child1 = parent1, child2 = parent2;
    auto [point1, point2] = crossoverPoints(parent1.path.size());
    for (size_t i = point1; i <= point2; ++i) {
        std::swap(child1.path[i], child2.path[i]);
    }
    fill(child2, parent1, point1, point2);
    fill(child1, parent2, point1, point2);
    return {child1, child2};
}

void fixPath(std::vector<int> &path) {
    std::unordered_multiset<int> used;
    std::unordered_set<int> notUsed;
    for (int i = 0; i < path.size(); ++i) notUsed.insert(i);
    for (const auto p: path) {
        used.insert(p);
        notUsed.erase(p);
    }
    if (notUsed.empty()) return;
    auto curNotUsed = notUsed.begin();
    for (auto &p: path) {
        if (used.count(p) == 1) continue;
        used.extract(p);
        p = *curNotUsed;
        curNotUsed++;
    }
}

std::pair<TIndividual, TIndividual> doCrossoverPMX(const TIndividual &parent1, const TIndividual &parent2) {
    TIndividual child1 = parent1, child2 = parent2;
    auto [point1, point2] = crossoverPoints(parent1.path.size());
    for (size_t i = point1; i <= point2; ++i) {
        std::swap(child1.path[i], child2.path[i]);
    }
    fixPath(child1.path);
    fixPath(child2.path);
    return {child1, child2};
}

void mutate(TIndividual &ind, double probability) {
    for (int i = 1; i < ind.path.size(); ++i) { // do not modify the start
        if (drand48() >= probability) continue;
        size_t swapIndex = rand() % ind.path.size();
        if (swapIndex == 0) continue;
        std::swap(ind.path[i], ind.path[swapIndex]);
    }
}

void printState(TMatrix *matrix, int generation) {
    printf("[%d]  Fitness: %f, Path len: %d\n",
           generation,
           individuals.at(0).fitness,
           calculatePathLength(individuals.at(0).path, matrix)
    );
}

std::vector<int> salesmanProblemGenetic(TMatrix *matrix, const Config &config) {
    int i, j;
    double mutation_probability = 0;
    double lastFitness = std::numeric_limits<double>::min();
    std::vector<int> best;
    double bestFitness;

    // initialization of random number generator
    srand(time(nullptr));
    srand48(time(nullptr));

    // born first population
    for (i = 0; i < config.population; i++) {
        TIndividual ind;

        // Generate some random path: Place city indexes to the vector in some
        // random order. At index 0 will be city we start from.
        ind.path.clear();
        ind.path.push_back(0);
        j = 1;
        while (j < matrix->getNumberOfTargets()) {
            long x = random() % matrix->getNumberOfTargets();
            auto p = find(ind.path.begin(), ind.path.end(), x);
            if (p == ind.path.end()) {
                ind.path.push_back((int) x);
                j++;
            }
        }
        // Store this path into table of individuals.
        // Fitness and other parameters will be computaed later.
        individuals.push_back(ind);
    }

    // compute fitnesses and sort individuals
    recalculate(matrix);
    printState(matrix, 0);

    // remember the best solution
    best = individuals.at(0).path;
    bestFitness = individuals.at(0).fitness;

    auto crossoverFn = config.crossoverMethod == CROSSOVER_METHOD_PMX ? doCrossoverPMX : doCrossoverOX;

    // run simulation
    for (i = 1; i < config.generations; i++) {
        std::vector<TIndividual> nextGeneration;

        for (int e = 0; e < config.eliteIndividuals; ++e) {
            nextGeneration.push_back(individuals[e]);
        }

        while (nextGeneration.size() != individuals.size()) {
            // selection: select individuals for a new generation
            size_t parent1 = rouletteWheelSelection(), parent2;
            do {
                parent2 = rouletteWheelSelection();
            } while (parent1 == parent2);

            if (drand48() > config.probabilityCrossover) continue;

            auto [child1, child2] = crossoverFn(individuals[parent1], individuals[parent2]);

            if (mutation_probability > 0) {
                mutate(child1, mutation_probability);
                mutate(child2, mutation_probability);
            }

            nextGeneration.push_back(child1);
            if (nextGeneration.size() != individuals.size()) nextGeneration.push_back(child2);
        }
        individuals = nextGeneration;

        // print the best result
        recalculate(matrix);
        printState(matrix, i);

        // if fitness < lastFitness, increase mutation probability by one step
        if (individuals.at(0).fitness < lastFitness) {
            mutation_probability += config.probabilityMutationStep;
        } else {
            mutation_probability = 0;
        }

        lastFitness = individuals.at(0).fitness;

        if (lastFitness > bestFitness) {
            best = individuals.at(0).path;
            bestFitness = individuals.at(0).fitness;
        }
    }

    return best;
}
