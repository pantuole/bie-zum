//-------------------------------------------------------------------
//
//  Template for solving the Travelling Salesman Problem
//  (c) 2021 Ladislava Smítková Janků <ladislava.smitkova@fit.cvut.cz>
//
//-------------------------------------------------------------------

#ifndef TMATRIX_H
#define TMATRIX_H

class TMatrix {
public:
    explicit TMatrix(const char *csvFileName);

    explicit TMatrix(int numberOfTargets_);

    ~TMatrix();

    [[nodiscard]] unsigned getNumberOfTargets() const;

    const char *getTargetName(int target);

    void setTargetName(int target, const char *name);

    int getDistance(int fromTarget, int toTarget);

    void setDistance(int fromTarget, int toTarget, int distance);

    void printMatrix();

    int getMaxPossibleDistance(int pathLen) const;

private:
    int maxDistance = 0;
    int numberOfTargets;
    int (*matrix)[];
    char *(*targetName)[];
};

#endif // TMATRIX_H
