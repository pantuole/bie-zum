//-------------------------------------------------------------------
//
//  Template for solving the Travelling Salesman Problem
//  (c) 2021 Ladislava Smítková Janků <ladislava.smitkova@fit.cvut.cz>
//
//  main.cpp: This source contains global function main,
//  which is the designated start of the program.
//
//-------------------------------------------------------------------

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <memory>

#include "bruteforce.h"
#include "genetic.h"
#include "path.h"
#include "tmatrix.h"

#define ALGO_UNKNOWN 0
#define ALGO_BRUTEFORCE 1
#define ALGO_GENETIC_OX 2
#define ALGO_GENETIC_PMX 3

void usage(char *appname) {
    char *app = strrchr(appname, '/');
    if (app == nullptr) app = appname;
    else app++;

    printf(
            "Template for solving the Travelling Salesman Problem.\n"
            "(c) 2021 Ladislava Smítková Janků <ladislava.smitkova@fit.cvut.cz>\n\n"
            "Usage: %s <distances-file-csv> <algorithm>\n\n"
            "<distances-file-csv>   CSV file with distances. Each line contains\n"
            "                       the name of the city and distances to other\n"
            "                       cities separated by commas.\n\n"
            "                       Example of the content of the input CSV file:\n"
            "                           Prague,0,2215,2292\n"
            "                           Madrid,2215,0,3982\n"
            "                           Ankara,2292,3982,0\n\n"
            "                       This means that Madrid is from Ankara 3982 km "
            "far,\n"
            "                       Prague from Madrid 2215 km far and Ankara from\n"
            "                       Prague 2292 km far.\n\n"
            "<algorithm>            Algorithm to be used:\n\n"
            "                       bruteforce  Use brute force; try all variants.\n"
            "                       genetic-ox  Use genetic algorithm (OX "
            "crossover).\n"
            "                       genetic-pmx Use genetic algorithm (PMX crossover).\n\n"
            "<population>\n"
            "<generations>\n"
            "<crossover probability>\n"
            "<mutation probability step>\n"
            "<elite individuals count>\n",
            app);
    exit(1);
}

void initConfig(Config &config, int argc, char *argv[], TCrossoverMethod method) {
    if (argc < 8) usage(argv[0]);
    config.population = std::stoi(argv[3]);
    config.generations = std::stoi(argv[4]);
    config.probabilityCrossover = std::stod(argv[5]);
    config.probabilityMutationStep = std::stod(argv[6]);
    config.eliteIndividuals = std::stoi(argv[7]);
    config.crossoverMethod = method;
}

int main(int argc, char *argv[]) {
    std::vector<int> result;

    // Parse arguments.

    if (argc < 3) usage(argv[0]);

    int algorithm = ALGO_UNKNOWN;
    if (strcmp(argv[2], "bruteforce") == 0)
        algorithm = ALGO_BRUTEFORCE;
    else if (strcmp(argv[2], "genetic-ox") == 0)
        algorithm = ALGO_GENETIC_OX;
    else if (strcmp(argv[2], "genetic-pmx") == 0)
        algorithm = ALGO_GENETIC_PMX;
    else
        usage(argv[0]);

    Config config;

    try {
        // Load matrix with distances and run the algorithm.
        auto m = new TMatrix(argv[1]);
        std::shared_ptr<TMatrix> matrix(m);
        matrix->printMatrix();

        printf("\nSolving...\n");

        switch (algorithm) {
            case ALGO_BRUTEFORCE:
                result = salesmanProblemBruteForce(matrix.get());
                break;
            case ALGO_GENETIC_OX:
                initConfig(config, argc, argv, CROSSOVER_METHOD_OX);
                result = salesmanProblemGenetic(matrix.get(), config);
                break;
            case ALGO_GENETIC_PMX:
                initConfig(config, argc, argv, CROSSOVER_METHOD_PMX);
                result = salesmanProblemGenetic(matrix.get(), config);
                break;
            default:
                throw std::invalid_argument("unknown algorithm");
        }

        printf("\nResult:\n");
        printPathWithNames(result, matrix.get());
        printf("\nPath length: %d\n", calculatePathLength(result, matrix.get()));
    } catch (const std::invalid_argument &err) {
        // Oops. Something wrong happened.
        fprintf(stderr, "Exception: %s\n", err.what());
        exit(1);
    }
}
