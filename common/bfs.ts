export interface IState {
    isValid(): boolean;

    next(): Iterable<IState>;

    equals(other: IState): boolean;

    toString(): string;
}

function reconstructPath(
    curState: string,
    initialKey: string,
    prev: Map<string, string[]>,
    path: string[]
) {
    if (initialKey === curState) {
        console.log("Solution found:");
        for (let i = path.length - 1; i >= 0; i--) {
            console.log(path[i]);
        }
        return;
    }
    for (const prevState of prev.get(curState)!) {
        if (path.includes(prevState)) continue;
        path.push(prevState);
        reconstructPath(prevState, initialKey, prev, path);
        path.pop();
    }
}

export function bfs(initialState: IState, finalState: IState, findAllSolutions: boolean) {
    const queue = [initialState];
    const initialKey = initialState.toString();
    const visited = new Set<string>([initialKey]);
    const prev = new Map<string, string[]>([[initialKey, []]]);

    let solutionFound = false;

    BFS: while (queue.length > 0) {
        const s = queue.shift()!;
        for (const nextState of s.next()) {
            const key = nextState.toString();
            if (!nextState.isValid()) continue;
            if (visited.has(key)) {
                if (findAllSolutions) prev.get(key)!.push(s.toString());
                continue;
            }
            visited.add(key);
            queue.push(nextState);
            prev.set(key, [s.toString()]);
            if (nextState.equals(finalState)) {
                solutionFound = true;
                break BFS;
            }
        }
    }

    if (solutionFound) {
        const finalKey = finalState.toString();
        reconstructPath(finalKey, initialKey, prev, [finalKey]);
    } else {
        console.log("No solution found.")
    }
}