export function equalArrays<T>(arr1: T[], arr2: T[]): boolean {
    return arr1.length == arr2.length && arr1.every((e, i) => e == arr1[i]);
}

export function arrToStr(arr: number[]) {
    return "[" + arr.reduce((acc, item) => acc + " " + item, "") + " ]";
}