package main

import (
	"container/list"
	"fmt"
	"math"
)

const BOTTLE1_SIZE = 3
const BOTTLE2_SIZE = 5

type State struct {
	b1 int
	b2 int
}

func (s State) Next() [6]State {
	var next = [6]State{}
	// empty one of the bottles
	next[0] = State{0, s.b2}
	next[1] = State{s.b1, 0}
	// fill fully one of the bottles
	next[2] = State{BOTTLE1_SIZE, s.b2}
	next[3] = State{s.b1, BOTTLE2_SIZE}
	// fill one of the bottles from another
	canMoveToSecond := int(math.Min(
		float64(s.b1),
		float64(BOTTLE2_SIZE-s.b2)))
	next[4] = State{s.b1 - canMoveToSecond, s.b2 + canMoveToSecond}
	canMoveToFirst := int(math.Min(
		float64(s.b2),
		float64(BOTTLE1_SIZE-s.b1)))
	next[5] = State{s.b1 + canMoveToFirst, s.b2 - canMoveToFirst}
	return next
}

func (s State) IsValid() bool {
	return s.b1 >= 0 && s.b1 <= BOTTLE1_SIZE &&
		s.b2 >= 0 && s.b2 <= BOTTLE2_SIZE
}

func (s State) Equals(other State) bool {
	return s.b1 == other.b1 && s.b2 == other.b2
}

func (s State) ToString() string {
	return fmt.Sprintf(`(%d %d)`, s.b1, s.b2)
}

func reconstructPath(prev map[string]State, finalState State) {
	cur := finalState
	path := []State{finalState}
	for {
		_, hasPrev := prev[cur.ToString()]
		if !hasPrev {
			break
		}
		cur = prev[cur.ToString()]
		path = append(path, cur)
	}
	for i := range path {
		fmt.Println(path[len(path)-i-1].ToString())
	}
}

func canFill(bottle1 int, bottle2 int) bool {
	var queue = list.New()
	var prev = make(map[string]State)
	var visited = make(map[string]bool)

	var initialState = State{0, 0}
	var finalState = State{bottle1, bottle2}

	queue.PushBack(initialState)
	visited[initialState.ToString()] = true

	for queue.Len() > 0 {
		current := queue.Front()
		queue.Remove(current)
		s := current.Value.(State)

		for _, nextState := range s.Next() {
			if !nextState.IsValid() || visited[nextState.ToString()] {
				continue
			}
			queue.PushBack(nextState)
			prev[nextState.ToString()] = s
			visited[nextState.ToString()] = true
			if nextState.Equals(finalState) {
				reconstructPath(prev, finalState)
				return true
			}
		}
	}

	return false
}

func main() {
	if canFill(0, 4) {
		fmt.Println("Can fill")
	} else {
		fmt.Println("Can not fill")
	}
}
