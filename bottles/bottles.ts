import { bfs, IState } from "../common/bfs"

const BOTTLE1_SIZE = 3;
const BOTTLE2_SIZE = 5;

class BottleState implements IState {
    constructor(
        private readonly b1: number,
        private readonly b2: number
    ) {
    }

    * next(): Iterable<BottleState> {
        // empty one of the bottles
        yield new BottleState(0, this.b2);
        yield new BottleState(this.b1, 0);
        // fill fully one of the bottles
        yield new BottleState(BOTTLE1_SIZE, this.b2);
        yield new BottleState(this.b1, BOTTLE2_SIZE);
        // fill one of the bottles from another
        const canMoveToSecond = Math.min(this.b1, BOTTLE2_SIZE - this.b2);
        yield new BottleState(this.b1 - canMoveToSecond, this.b2 + canMoveToSecond);
        const canMoveToFirst = Math.min(this.b2, BOTTLE1_SIZE - this.b1);
        yield new BottleState(this.b1 + canMoveToFirst, this.b2 - canMoveToFirst);
    }

    isValid() {
        return (
            this.b1 >= 0 && this.b1 <= BOTTLE1_SIZE &&
            this.b2 >= 0 && this.b2 <= BOTTLE2_SIZE
        );
    }

    equals(other: BottleState) {
        return this.b1 === other.b1 && this.b2 === other.b2;
    }

    toString() {
        return `(${this.b1} ${this.b2})`;
    }
}

const initialState = new BottleState(0, 0);
const finalState = new BottleState(0, 4);

bfs(initialState, finalState, true);