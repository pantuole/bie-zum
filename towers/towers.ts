import { bfs, IState } from "../common/bfs";
import { arrToStr, equalArrays } from "../common/utils";

class State implements IState {
    constructor(
        private readonly t1: number[],
        private readonly t2: number[],
        private readonly t3: number[]
    ) {
    }

    private static isValidTower(towers: number[]) {
        return towers.every((d, i) => i + 1 == towers.length || d > towers[i + 1]);
    }

    isValid(): boolean {
        return State.isValidTower(this.t1) && State.isValidTower(this.t2) && State.isValidTower(this.t3);
    }

    // move the smallest disk from the first tower to others
    private static move(t1: number[], t2: number[], t3: number[]): State[] {
        if (t1.length === 0) return [];
        const nextT1 = [...t1];
        const last = nextT1.pop()!;
        return [
            new State(nextT1, [...t2, last], t3),
            new State(nextT1, t2, [...t3, last]),
        ];
    }

    * next(): Iterable<State> {
        // try all moves (from t1, t2, t3)
        for (const s of State.move(this.t1, this.t2, this.t3)) yield s;
        for (const s of State.move(this.t2, this.t1, this.t3)) yield new State(s.t2, s.t1, s.t3);
        for (const s of State.move(this.t3, this.t1, this.t2)) yield new State(s.t2, s.t3, s.t1);
    }

    equals(other: State): boolean {
        return equalArrays(this.t1, other.t1) && equalArrays(this.t2, other.t2) && equalArrays(this.t3, other.t3);
    }

    toString(): string {
        return arrToStr(this.t1) + " " + arrToStr(this.t2) + " " + arrToStr(this.t3);
    }
}

const initialState = new State([4, 3, 2, 1], [], []);

const finalState = new State([], [], [4, 3, 2, 1]);

bfs(initialState, finalState, false);